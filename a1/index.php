<?php

require "code.php";


?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP OOP | Activity</title>
</head>
<body>

    <?php $newProduct->setStockNo(3); ?>
    <?php $newMobile->setStockNo(5); ?>
    <?php $newComputer->setCategory("laptops, computers, and electronics"); ?>


    <ul>
        <h1><li>Print Product Here</li></h1>
        <p><?php echo $newProduct->printDetails(); ?></p>

        <h1><li>Print Mobile Here</li></h1>
        <p><?php echo $newMobile->printDetails(); ?></p>
        
        <h1><li>Print Computer Here</li></h1>
        <p><?php echo $newComputer->printDetails(); ?></p>
    </ul>
</body>
</html>