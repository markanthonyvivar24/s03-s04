<?php 

class Product {

    public $name;
    public $price;
    public $short_description;
    public $description;
    public $stock_no;
    public $category;
     

    public function __construct($name, $price, $short_description,
    $stock_no, $category, ){
        $this->name = $name;
        $this->price = $price;
        $this->short_description = $short_description;
        $this->stock_no = $stock_no; 
        $this->category = $category;
        
    }


    public function printDetails() {
        return "The product has name of $this->name and it's price is
        $this->price, and the stock no is $this->stock_no";
    }

    public function getPrice() {
        return $this->price;
    }

    public function getStockNo() {
        return $this->stock_no;
    }

    public function getCategory() {
        return $this->category;
    }


    public function setPrice($price) {
        $this->price = $price;
    }

    public function setStockNo($stock) {
        $this->stock_no = $stock;
    }

    public function setCategory($category) {
        $this->category = $category;
    }
}


class Mobile extends Product { 

    public function printDetails() {
        return "Our latest mobile is $this->name with a cheapest price
        of $this->price";
    }

}


class Computer extends Product {


    public function printDetails() {
        return "Our newest computer is $this->name and its base price is 
        $this->price";
    }


}



$newProduct = new Product("Xiaomi Mi Monitor", 22000.00, "Good for gaming and  for coding",
5, "computer-peripherals");


$newMobile = new Mobile("Xiaomi Redmi Note 10 pro", 13590.00, "Latest Xioami phone ever made",
10, "mobiles and electronics");


$newComputer = new Computer("HP Business Laptop", 29000.00, "HP Laptop only made for business",
10, "laptops and computer"); 