<?php require "./code.php" ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP Classes and Objects</title>
</head>
<body>
    <h1>PHP Classes and Objects</h1>
    <!-- <pre>
        <?php print_r($buildingObj); ?>
    </pre> -->

    <pre>
        <?php print_r($newBuilding); ?>
    </pre>

    <pre>
        <?php print_r($newPerson); ?>
    </pre>


    <ul>
        
        <p>
            <li><?php echo $newPerson->getPersonDetails() ?></li>
        </p>

      

        <p>
            <?php $newPerson->setFirstName("Peter"); ?>
        </p>

        <p>
            <li> <?php echo $newPerson->getFirstName(); ?></li>
        </p>


        <p>
            <?php $newPerson->setMiddleName("Masipag"); ?>
        </p>

        <p>
            <li><?php echo $newPerson->getMiddleName(); ?></li>
        </p>

        <p>
            <?php $newPerson->setLastName("Penduko"); ?>
        </p>

        <p>
            <li><?php echo $newPerson->getLastName(); ?></li>
        </p>


        <p>
            <?php echo $newPerson2->getPersonDetails(); ?>
        </p>
    </ul>

</body>
</html>