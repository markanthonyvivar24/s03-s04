<?php


// $buildingObj = (object)[
//     "name" => "Caswynn Building",
//     "floors" => 8,
//     "address" => (object)[
//         "barangay" => "Sacred Heart",
//         "city" => "Quezon City",
//         "country" => "Philippines"
//     ]
// ];


class Building {

    public $name;
    public $floors;
    public $address;

    public function __construct($nameValue, $floorsValue, $addressValue){
        $this->name = $nameValue; 
        $this->floors = $floorsValue; 
        $this->address = $addressValue; 
    }
}




$newBuilding = new Building("Enzo", 10, "Commonwealth Q.C.");



class Person {
    public $firstName;
    public $middleName;
    public $lastName;
    public $age;
    public $birthday;



    public function __construct($firstNameValue, $middleNameValue, $lastNameValue, $ageValue, $birthdayValue)
    {

        $this->firstName = $firstNameValue;
        $this->middleName = $middleNameValue;
        $this->lastName = $lastNameValue;
        $this->age = $ageValue;
        $this->birthday =$birthdayValue;
    }


    public function getPersonDetails(){
        return "$this->firstName, $this->middleName,
        $this->lastName, $this->age, $this->birthday";
    }

    public function getFirstName(){
        return $this->firstName;
    }

    
    public function getMiddleName(){
        return $this->middleName;
    }

    public function getLastName(){
        return $this->lastName;
    }


    public function setFirstName($fName){
        $this->firstName = $fName;
    }

    public function setMiddleName($mName){
        $this->middleName = $mName;
    }

    public function setLastName($lName){
        $this->lastName = $lName;
    }

}



class Person2 extends Person {


    public function getPersonDetails(){
        return "$this->lastName, $this->age";
    }


}


$newPerson = new Person("Mark Anthony", "Pempengco", "Vivar", 20, "10-20-94" );
$newPerson2 = new Person2("danica","asd","delos reyes",33,"123123");